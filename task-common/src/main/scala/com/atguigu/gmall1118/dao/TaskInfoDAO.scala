package com.atguigu.gmall1118.dao

import com.atguigu.gmall1118.bean.{TagInfo, TaskInfo}
import com.atguigu.gmall1118.util.MysqlUtil

object TaskInfoDAO {


  def getTaskInfo(taskId:String): TaskInfo ={

    val sql=s" select * from task_info where id='$taskId'"
    val maybeInfo: Option[TaskInfo] = MysqlUtil.queryOne(sql , classOf[TaskInfo], true);
    if(maybeInfo ==None){
      null
    }else{
      maybeInfo.get
    }
  }

}
