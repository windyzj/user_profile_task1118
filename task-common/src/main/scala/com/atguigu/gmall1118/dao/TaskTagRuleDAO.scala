package com.atguigu.gmall1118.dao

import com.atguigu.gmall1118.bean.TaskTagRule
import com.atguigu.gmall1118.util.MysqlUtil

object TaskTagRuleDAO {


  def getTaskTagRuleList(taskId:String): List[TaskTagRule] ={

    val sql=s" select tr.id, tr.tag_id,tr.task_id,tr.query_value,tr.sub_tag_id ,ti.tag_name  sub_tag_value  " +
      s"from task_tag_rule tr join tag_info ti on tr.sub_tag_id=ti.id where tr.task_id= ${taskId}"
    val taskTagRules: List[TaskTagRule] = MysqlUtil.queryList(sql, classOf[TaskTagRule], true)
    taskTagRules

  }

}
