package com.atguigu.gmall1118.dao

import com.atguigu.gmall1118.bean.TagInfo
import com.atguigu.gmall1118.util.MysqlUtil

object TagInfoDAO {


  def getTagInfoBytaskId(taskId:String): TagInfo ={

    val maybeInfo: Option[TagInfo] = MysqlUtil.queryOne(s"select * from tag_info where tag_task_id=${taskId}", classOf[TagInfo], true);
    if(maybeInfo ==None){
       null
    }else{
       maybeInfo.get
    }
  }

  def getTagInfoListWithOn(): List[TagInfo] ={
    val sql=s" select ti.*  from tag_info ti join task_info tk on ti.tag_task_id=tk.id  where tk.task_status='1'"
    val tagInfoList: List[TagInfo] = MysqlUtil.queryList(sql, classOf[TagInfo], true)
    tagInfoList
  }



}
